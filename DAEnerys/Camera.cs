﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Drawing;
using System.Windows.Forms;
using Extensions;

namespace DAEnerys
{
    public class Camera
    {
        public Vector3 Position = new Vector3(0, 0, 15);
        public Vector3 Orientation = new Vector3((float)Math.PI, 0f, 0f);

        public float NearClipDistance = 0.01f;
        public float ClipDistance = 1000;
        public float FieldOfView = 0.9599f;

        public float MinZoom = 0.01f;
        public float MaxZoom = 15.0f;

        private bool orthographic;
        public bool Orthographic { get { return orthographic; } set { orthographic = value; Update(true); } }

        private float orthographicSizeTarget = 2;
        private float oldOrthographicSizeTarget = 2;
        private float perspectiveZoom = 1;

        private float orthographicSize = 2;
        public float OrthographicSize { get { return orthographicSize; } }

        private float lastOrthographicSize;

        public float CalculatedZoom = 10;
        public float ZoomSpeed = 10;

        public Vector3 LookAt = Vector3.Zero;
        public Vector3 Direction
        {
            get
            {
                return Program.Camera.LookAt - Program.Camera.Position;
            }
        }

        public bool SmoothZooming = true;

        private float oldZoomTarget = 10;
        private float zoomTarget = 10;
        public float ZoomTarget { get { return zoomTarget; } set { zoomTarget = value; Update(); } }

        private float zoom = 10;

        private float lastZoom;
        private Vector2 angles = new Vector2((float)Math.PI, (float)Math.PI);

        private float lastWheelPrecise;

        private Point lastPos;
        private Point pressPos;

        //private Cursor cursor = new Cursor(Program.main.Cursor.Handle);

        public void MouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MouseState mouse = Mouse.GetState();
                Cursor.Hide();
                pressPos = Cursor.Position;
            }
        }

        public void MouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //Vector2 newPos = new Vector2(Program.GLControl.Bounds.Left + pressPos.X, Program.GLControl.Bounds.Top + pressPos.Y);
                //Mouse.SetPosition(newPos.X, newPos.Y);
                Cursor.Position = pressPos;
                Cursor.Show();
                pressPos = Point.Empty;
            }
        }

        public void KeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (ActionKey.IsDown(Action.TOGGLE_ORTHOGRAPHIC)) //Toggle orthographic view
            {
                this.Orthographic = !this.Orthographic;
                if (this.Orthographic)
                {
                    this.perspectiveZoom = this.ZoomTarget;
                    this.ZoomTarget = CalculatedZoom;
                }
                else
                    this.ZoomTarget = perspectiveZoom;

                Program.main.UpdatePerspectiveOrthoCombo();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_FRONT))
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)Math.PI;

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_BACK))
            {
                angles.X = (float)Math.PI;
                angles.Y = 0;

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_LEFT))
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)-Math.PI / 2;

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_RIGHT))
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)Math.PI / 2;

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_TOP))
            {
                angles.X = (float)Math.PI * 1.5f;
                angles.Y = (float)Math.PI;

                angles.X = (float)Utilities.Clamp(angles.X, Math.PI - Math.PI / 2, (Math.PI + Math.PI / 2) - 0.000001f);

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.VIEW_BOTTOM))
            {
                angles.X = 0;
                angles.Y = (float)Math.PI;

                angles.X = (float)Utilities.Clamp(angles.X, Math.PI - Math.PI / 2, (Math.PI + Math.PI / 2) - 0.000001f);

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }
            else if (ActionKey.IsDown(Action.CAM_RESET))
            {
                angles.X = (float)Math.PI;
                angles.Y = (float)Math.PI;

                this.Orthographic = false;

                this.ZoomTarget = CalculatedZoom;
                this.perspectiveZoom = this.ZoomTarget;
                this.orthographicSizeTarget = 16;

                UpdatePosition();
                Renderer.InvalidateView();
                Renderer.Invalidate();
                Program.main.UpdatePerspectiveOrthoCombo();
            }
        }

        public void Update(bool forceUpdate = false)
        {
            if (Program.GLControl == null)
                return;

            MouseState mouse = Mouse.GetState();
            Point position = Cursor.Position;

            if (Program.GLControl.Focused || forceUpdate)
            {
                float zoomDelta = mouse.WheelPrecise - lastWheelPrecise;

                if (mouse.RightButton == OpenTK.Input.ButtonState.Pressed || forceUpdate)
                {
                    float deltaX = position.X - lastPos.X;
                    float deltaY = position.Y - lastPos.Y;

                    angles.X += deltaY * 0.01f;
                    angles.Y -= deltaX * 0.01f;

                    angles.X = (float)Utilities.Clamp(angles.X, Math.PI - Math.PI / 2, (Math.PI + Math.PI / 2) - 0.000001f);

                    Renderer.InvalidateView();
                    Renderer.Invalidate();
                }

                if (!this.Orthographic)
                {
                    zoomTarget -= zoomDelta * ZoomSpeed * 0.01f;
                    zoomTarget = Utilities.Clamp(zoomTarget, MinZoom, MaxZoom);
                }
                else
                {
                    orthographicSizeTarget += zoomDelta * (orthographicSizeTarget / 30);
                    orthographicSizeTarget = Utilities.Clamp(orthographicSizeTarget, 0.0001f, 500);
                }
            }

            UpdateZoom();
            UpdateOrthographicSize();
            UpdatePosition();

            if (lastZoom != zoomTarget)
            {
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }

            if (lastOrthographicSize != orthographicSizeTarget)
            {
                Renderer.InvalidateView();
                Renderer.Invalidate();
            }

            lastPos = position;
            lastZoom = zoomTarget;
            lastOrthographicSize = orthographicSizeTarget;
            lastWheelPrecise = mouse.WheelPrecise;
        }

        private void UpdateZoom()
        {
            float diff = Math.Abs(zoom - zoomTarget);
            if (diff < 0.001f)
                return;

            if (SmoothZooming)
                zoom = Utilities.SmoothStepChange(zoom, oldZoomTarget, zoomTarget, (float)Program.ElapsedSeconds * 6, 2);
            else
                zoom = zoomTarget;

            oldZoomTarget = zoomTarget;

            Renderer.InvalidateView();
            Renderer.Invalidate();
        }

        private void UpdateOrthographicSize()
        {
            float diff = Math.Abs(orthographicSize - orthographicSizeTarget);
            if (diff < 0.001f)
                return;

            if (SmoothZooming)
                orthographicSize = Utilities.SmoothStepChange(orthographicSize, oldOrthographicSizeTarget, orthographicSizeTarget, (float)Program.ElapsedSeconds * 6, 2);
            else
                orthographicSize = orthographicSizeTarget;

            oldOrthographicSizeTarget = orthographicSizeTarget;

            Renderer.InvalidateView();
            Renderer.Invalidate();
        }

        private void UpdatePosition()
        {
            Position = LookAt + Vector3.Transform(new Vector3(0, 0, zoom), Matrix3.CreateRotationX(angles.X) * Matrix3.CreateRotationY(angles.Y));
        }

        public Matrix4 GetViewMatrix()
        {
            return Matrix4.LookAt(Position, LookAt, new Vector3(0, 1, 0));
        }
    }
}