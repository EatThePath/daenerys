Feel free to post pull requests.

You can contact me here on BitBucket or on the Gearbox Forums.
If you find a bug or have some idea, put it in the Issues section.